<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentDetail extends Model
{
    protected $table = 'document_detail';

    protected $fillable = [
        'id_document', 'id_profile'
    ];
}
