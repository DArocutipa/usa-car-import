<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentPurchases extends Model
{
    protected $table = 'payment_purchases';
    
    protected $fillable = [
        'id_purchases', 'id_payment', 'fecha'
    ];
}
