<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Profile;

class UserController extends Controller
{

    public function index($id){
       $model = Profile::where('id', '=', $id)->firstOrFail();
       return view('users.profile',[ 'details' => $model]);
    }

}
