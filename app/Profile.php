<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    
    protected $table = 'profiles';

    protected $fillable = [
        'id_user', 'ruc', 'razon_social','direccion_fiscal','direccion','cuentas_bancarias','oficinas','estado'
    ];
}
