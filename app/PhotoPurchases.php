<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoPurchases extends Model
{
    protected $table = 'photo_purchases';
    
    protected $fillable = [
        'id_purchases', 'descripcion'
    ];
}
