<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    protected $table = 'purchases';
    
    protected $fillable = [
        'id_profile', 'id_fotos_purchases', 'nombre','descripcion','tracking_number','empresa_correo_carrier','foto_voucher_tienda','foto_voucher_importacion','estado'
    ];
}
