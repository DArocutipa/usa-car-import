<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
    protected $table = 'payment_details';
    
    protected $fillable = [
        'id_payment', 'document', 'date'
    ];
}
