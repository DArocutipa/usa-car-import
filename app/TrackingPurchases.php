<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingPurchases extends Model
{
    protected $table = 'tracking_purchases';

    protected $fillable = [
        'id_empresa', 'id_marca', 'id_detalle_fotos','id_tracking','codigo_vin','anio_fabricacion','color','puerto_salida','puerto_entrada','direccion_entrega','comentarios'
    ];
}
