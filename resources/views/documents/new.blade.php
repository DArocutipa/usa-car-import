@extends('layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
    type="text/css" />
    
<link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
   
</div>
@endsection

@section('content')
<!-- select -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                      
                        <div class="dropdown d-inline">                        

                            <h4 class="mb-1 mt-0">Documentos Registrados</h4>   
                            
                           
                        </div>
                    </div>
                    <div class="col text-right">
                       <a href="/documents/list"><button class="btn btn-primary mt-2 mr-1" id="btn-new-event"><i data-feather="arrow-left-circle"></i> 
                            Volver</button></a> 
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              
                
                <form action="#" method="post" class="needs-validation p-5" novalidate>
               
               
                    <div class="row mt-2">
                       
                         
                        <div class="col-lg-6">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Cliente</label>                               
                                <select data-plugin="customselect" id="cliente" name="cliente" class="form-control" data-placeholder="">
                                    <option></option>
                                    <option value="0">Juan Diego Apaza</option>                                    
                                </select>
                               
                            </div>                        
                        </div>  
                        <div class="col-lg-6">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Vehiculo</label>                               
                                <select data-plugin="customselect" id="empresa" name="empresa" class="form-control" data-placeholder="">
                                    <option></option>
                                    <option value="0">Mazda Demio 2008</option>
                                </select>
                               
                            </div>                        
                        </div>  
                   
                        
                </div>
          
                <div class="clsbox-1" runat="server" style="text-align: center;">
                    <div class="dropzone clsbox" id="mydropzone" style="background:#e8f0fe !important;">
                        <i class="h1 text-muted  uil-cloud-upload"></i>
                        <h3>Suelte los archivos aquí o haga clic para cargar.</h3>
                    </div>
                </div>
 <div class="col-lg-12 form-group mt-4">
                       
                        <button class="btn btn-primary" type="button">Guardar Datos</button>
                    </div>
               
            </form>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col -->
</div>
<!-- end row -->



<!-- end row -->
@endsection

@section('script')
<!-- Plugins Js -->
<script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>

<script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
<script>
    Dropzone.autoDiscover = false;

// Dropzone class:
var myDropzone = new Dropzone("div#mydropzone", { url: "/file/post"});

// If you use jQuery, you can use the jQuery plugin Dropzone ships with:
$("div#myDrop").dropzone({ url: "/file/post" });
</script>
@endsection

@section('script-bottom')
<script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection
