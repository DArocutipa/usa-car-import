@extends('layouts.vertical')


@section('css')
<!-- plugin css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
     
     
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                      
                        <div class="dropdown d-inline">                        

                            <h4 class="mb-1 mt-0">Clientes Registrados</h4>   
                            
                           
                        </div>
                    </div>
                    <div class="col text-right">
                       <a href="/users/customers/new"><button class="btn btn-primary mt-2 mr-1" id="btn-new-event"><i data-feather="plus-circle" ></i> 
                           Crear Cliente</button></a> 
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
    
        <div class="col-12">
            <div class="card">
                <div class="card-body">               
                    

                    <table id="basic-datatable" class="table table-striped dt-responsive nowrap">
                       
                        <thead>
                            <tr>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Cedula</th>
                                <th>Empresa</th>
                                <th>Pais</th>
                                <th width="20">Controles</th>
                            </tr>
                        </thead>
                    
                    
                        <tbody>
                            <tr>
                                <td>Tiger</td>
                                <td>Nixon Nixon</td>
                                <td>54156214</td>
                                <td>System Architect</td>
                                <td>Perú</td>
                                <td> 
                                    <a href="#" class="btn btn-primary btn-sm">Editar</a>
                                    <a href="#" class="btn btn-warning btn-sm">Suspender</a>
                                    <a href="#" class="btn btn-danger btn-sm">Eliminar</a>
                                </td>
                            </tr>
                           
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->


        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('script')
<!-- datatable js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
@endsection

@section('script-bottom')
<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection