@extends('layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
    type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
   
</div>
@endsection

@section('content')
<!-- select -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                      
                        <div class="dropdown d-inline">                        

                            <h4 class="mb-1 mt-0">Clientes Registrados</h4>   
                            
                           
                        </div>
                    </div>
                    <div class="col text-right">
                       <a href="/users/customers/list"><button class="btn btn-primary mt-2 mr-1" id="btn-new-event"><i data-feather="arrow-left-circle"></i> 
                            Volver</button></a> 
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="#"  class="needs-validation p-5" novalidate>
                    <div class="row mt-2">
                       
                         
                        <div class="col-lg-3">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Tipo de Documento</label>                               
                                    <select id="tipodocumento" name="tipodocumento" class="form-control custom-select">
                                        <option>DNI</option>
                                        <option>Carnet de Extranjeria</option>
                                        <option>Pasaporte</option>
                                    </select>
                               
                            </div>                        
                        </div>  
                        <div class="col-lg-3">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Documento</label>
                                <input type="text" id="documento" name="documento" class="form-control">
                               
                            </div>                        
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Nombres</label>
                                <input type="text" id="nombres" name="nombres" class="form-control">
                            </div>                        
                        </div>       
                       
                    <div class="col-lg-7">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Apellidos</label>
                            <input type="text" id="apellidos" name="apellidos" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Dirección</label>
                            <input type="text" id="direccion" name="direccion" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Correo Electrónico</label>
                            <input type="email" id="correo" name="correo" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Contraseña</label>
                            <input type="password" id="clave" name="clave" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Telefono</label>
                            <input type="text" id="telefono" name="telefono" class="form-control">
                        </div>                        
                    </div>                    
                    <div class="col-lg-4">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Ciudad</label>
                            <input type="text" id="ciudad" name="ciudad" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Foto de Documento Frontal</label>
                            <input type="file" id="fotofrontal" name="fotofrontal" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Foto de Documento Posterior</label>
                            <input type="file" id="fotoposterior" name="fotoposterior" class="form-control">
                        </div>                        
                    </div>


                   
                    <div class="col-lg-12 form-group mb-3">
                       
                        <button class="btn btn-primary" type="button">Guardar Datos</button>
                    </div>
                        
                </div>
            </form>
                
                    
                
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col -->
</div>
<!-- end row -->



<!-- end row -->
@endsection

@section('script')
<!-- Plugins Js -->
<script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
@endsection

@section('script-bottom')
<script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection