@extends('layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
    type="text/css" />
    
<link href="{{ URL::asset('assets/libs/summernote/summernote.min.css') }}" rel="stylesheet" />
@endsection

@section('breadcrumb')
<div class="row page-title">
   
</div>
@endsection

@section('content')
<!-- select -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                      
                        <div class="dropdown d-inline">                       

                            <h4 class="mb-1 mt-0">Perfil</h4>   
                            
                           
                        </div>
                    </div>
                   
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              
                <form action="#"  class="needs-validation p-5" novalidate>
                    <div class="row mt-2">
                        <div class="col-lg-3">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Ruc</label>
                                <input type="hidden" id="id" name="id" value="{{ $details->id }}" class="form-control">
                                <input type="text" id="ruc" name="ruc" value="{{ $details->ruc }}" class="form-control">
                               
                            </div>                        
                        </div>
                        <div class="col-lg-9">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Razón Social</label>
                                <input type="text" id="razon_social" name="razon_social" value="{{ $details->razon_social }}" class="form-control">
                            </div>                        
                        </div>       
                       
                    <div class="col-lg-7">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Dirección Fiscal</label>
                            <input type="text" id="direccion_fiscal" name="direccion_fiscal" value="{{ $details->direccion_fiscal }}" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group mt-3 mt-sm-0">
                            <label>Dirección</label>
                            <input type="text" id="direccion" name="direccion" value="{{ $details->direccion }}" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-lg-6">
                        <label>Cuentas Bancarias</label>
                        <div id="cuentas_bancarias" name="cuentas_bancarias">{{ $details->cuentas_bancarias }}
                            
                        </div>               
                    </div>
                    <div class="col-lg-6">
                        <label>Oficinas</label>
                        <div id="oficinas" name="oficinas">{{ $details->oficinas }}
                            
                        </div>               
                    </div>
                   
                    <div class="col-lg-12 form-group mb-3">
                       
                        <button class="btn btn-primary" type="button">Guardar Datos</button>
                    </div>
                        
                </div>
            </form>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col -->
</div>
<!-- end row -->



<!-- end row -->
@endsection

@section('script')

<script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
@endsection



@section('script-bottom')
<!-- Validation init js-->

<script src="{{ URL::asset('assets/js/pages/form-editor.init.js') }}"></script>
@endsection
