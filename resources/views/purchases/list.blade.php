@extends('layouts.vertical')


@section('css')
<!-- plugin css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
     
     
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                      
                        <div class="dropdown d-inline">                        

                            <h4 class="mb-1 mt-0">Compras Registradas</h4>   
                            
                           
                        </div>
                    </div>
                    <div class="col text-right">
                       <a href="/purchases/new"><button class="btn btn-primary mt-2 mr-1" id="btn-new-event"><i data-feather="plus-circle" ></i> 
                           Registrar Compra</button></a> 
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
    
        <div class="col-12">
            <div class="card">
                <div class="card-body">               
                    

                    <table id="basic-datatable" class="table table-striped dt-responsive nowrap">
                       
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Tienda</th>
                                <th>Empresa de Correo</th>
                                <th>Fecha Pedido</th>
                                <th>Estado</th>
                                <th width="20">Controles</th>
                            </tr>
                        </thead>
                    
                    
                        <tbody>
                            <tr>
                                <td>Monitor</td>
                                <td>Amazon</td>
                                <td>DHL</td>
                                <td>2020-07-28</td>
                                <td>Proceso</td>
                                <td>
                                 
                                    <a href="#" class="btn btn-success btn-sm" data-toggle="modal"
                                    data-target="#myModal">Estado</a>
                                    <a href="#" class="btn btn-primary btn-sm">Editar</a>
                                    <a href="#" class="btn btn-warning btn-sm">Suspender</a>
                                    <a href="#" class="btn btn-danger btn-sm">Eliminar</a>
                                   
                                   {{--  <i data-feather="edit-3"></i> 
                                    <i data-feather="delete"></i> 
                                    <i data-feather="stop-circle"></i>  --}}
                                </td>
                            </tr>
                           
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">Tracking</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row p-3">
                                    <div class="col-lg-5">
                                        <div class="form-group mt-3 mt-sm-0">
                                            <label>Estado</label>                               
                                                <select id="tipodocumento" name="tipodocumento" class="form-control custom-select">
                                                    <option>En Proceso</option>
                                                    <option>Recibido</option>
                                                    <option>Aduanas</option>
                                                    <option>Listo para Recoger</option>
                                                    <option>Entregado</option>
                                                </select>
                                           
                                        </div>                        
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="form-group mt-3 mt-sm-0">
                                            <label>Comentario</label>                               
                                               <textarea class="form-control" name="" id="" cols="5" rows="5"></textarea>
                                           
                                        </div>                        
                                    </div>
                                    <div class="offset-lg-9 col-lg-3">
                                    <button type="button" class="btn btn-primary">Registrar</button>
                                    </div>
                                </div>
                                <div class="left-timeline pl-4">
                                    <ul class="list-unstyled events">
                                        <li class="event-list">
                                            <div>
                                                <div class="media">
                                                    <div class="event-date text-center mr-4">
                                                        <div class=" avatar-sm rounded-circle bg-soft-primary">
                                                            <span class="font-size-16 avatar-title text-primary font-weight-semibold">
                                                                02
                                                            </span>
                                                        </div>
                                                        <p class="mt-2">Jun</p>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="card d-inline-block">
                                                            <div class="card-body">
                                                                <h5 class="mt-0">En Proceso</h5>
                                                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                        
                                        <li class="event-list">
                                            <div>
                                                <div class="media">
                                                    <div class="event-date text-center mr-4">
                                                        <div class=" avatar-sm rounded-circle bg-soft-primary">
                                                            <span class="font-size-16 avatar-title text-primary font-weight-semibold">
                                                                03
                                                            </span>
                                                        </div>
                                                        <p class="mt-2">Jun</p>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="card d-inline-block">
                                                            <div class="card-body">
                                                                <h5 class="mt-0">Recibido</h5>
                                                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>  
                        
                                                              
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="event-list">
                                            <div>
                                                <div class="media">
                                                    <div class="event-date text-center mr-4">
                                                        <div class=" avatar-sm rounded-circle bg-soft-primary">
                                                            <span class="font-size-16 avatar-title text-primary font-weight-semibold">
                                                                04
                                                            </span>
                                                        </div>
                                                        <p class="mt-2">Jun</p>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="card d-inline-block">
                                                            <div class="card-body">
                                                                <h5 class="mt-0">Transito</h5>
                                                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>  
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="event-list">
                                            <div>
                                                <div class="media">
                                                    <div class="event-date text-center mr-4">
                                                        <div class=" avatar-sm rounded-circle bg-soft-primary">
                                                            <span class="font-size-16 avatar-title text-primary font-weight-semibold">
                                                                05
                                                            </span>
                                                        </div>
                                                        <p class="mt-2">Jun</p>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="card d-inline-block">
                                                            <div class="card-body">
                                                                <h5 class="mt-0">Aduanas</h5>
                                                                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="event-list">
                                            <div>
                                                <div class="media">
                                                    <div class="event-date text-center mr-4">
                                                        <div class=" avatar-sm rounded-circle bg-soft-primary">
                                                            <span class="font-size-16 avatar-title text-primary font-weight-semibold">
                                                                06
                                                            </span>
                                                        </div>
                                                        <p class="mt-2">Jun</p>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="card d-inline-block">
                                                            <div class="card-body">
                                                                <h5 class="mt-0">Listo para recojo</h5>
                                                               <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                        
                                    </ul>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
                                
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
    </div>
    <!-- end row-->


        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('script')
<!-- datatable js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
@endsection

@section('script-bottom')
<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
