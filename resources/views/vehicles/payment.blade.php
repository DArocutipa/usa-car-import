@extends('layouts.vertical')

@section('css')
<!-- plugin css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
   
</div>
@endsection

@section('content')
<!-- select -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                      
                        <div class="dropdown d-inline">                        

                            <h4 class="mb-1 mt-0">Detalle de Pagos del Vehiculo</h4>   
                            
                           
                        </div>
                    </div>
                    <div class="col text-right">
                       <a href="/vehicles/list"><button class="btn btn-primary mt-2 mr-1" id="btn-new-event"><i data-feather="arrow-left-circle"></i> 
                            Volver</button></a> 
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              
                
                <form action="#" method="post" class="needs-validation p-5" novalidate>
                    <div class="row">
                        <div class="col-lg-6">    
                    <h1 class="">Precio Venta: $ 20.000</h1>
                    
                        </div>
                        <div class="offset-lg-1 col-lg-5"> 
                            <h1>Monto Actual: $ 5.000</h1>    
                        <h5>Cuotas a Pagar: 5</h5>
                        <h5>Cuotas Pagadas: 1</h5>
                </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Fecha</label>
                                <input type="date" id="marca" name="marca" class="form-control">
                               
                            </div>                        
                        </div> 
                    </div>
                    <div class="row mt-2">
                       
                        <div class="col-lg-4">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Tipo de Transacción</label>
                                <input type="text" id="marca" name="marca" class="form-control">
                               
                            </div>                        
                        </div>    
                         <div class="col-lg-4">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Monto</label>
                                <input type="text" id="marca" name="marca" class="form-control">
                               
                            </div>                        
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mt-3 mt-sm-0">
                                <label>Documento</label>
                                <input type="file" id="modelo" name="modelo" class="form-control">
                            </div>                        
                        </div>  
                              
            

                        <div class="col-lg-3 form-group mt-4">
                       
                            <button class="btn btn-primary" type="button">Registrar Pago</button>
                        </div>
                   
                        
                </div>
          
      

               
            </form>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
        <div class="col-12">
            <div class="card">
                <div class="card-body">               
                    

                    <table id="basic-datatable" class="table table-striped dt-responsive nowrap">
                       
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Tipo de Transacción</th>
                                <th>Monto</th>
                                <th width="20">Controles</th>
                            </tr>
                        </thead>
                    
                    
                        <tbody>
                            <tr>
                                <td>28-07-2020</td>
                                <td>Deposito</td>
                                <td>$ 5.000 </td>
                                <td>
                                  
                                    <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Ver Voucher</a>
                                    <a href="#" class="btn btn-warning btn-sm">Suspender</a>
                                    <a href="#" class="btn btn-danger btn-sm">Eliminar</a>
                                   
                                   {{--  <i data-feather="edit-3"></i> 
                                    <i data-feather="delete"></i> 
                                    <i data-feather="stop-circle"></i>  --}}
                                </td>
                            </tr>
                           
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div> <!-- end col -->
</div>
<!-- end row -->


<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">Voucher</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img src="https://i.pinimg.com/564x/73/ce/4a/73ce4a1834085faa7827c1b1e818d3aa.jpg" alt="" class="responsive" style="width: 100%">
                               
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
                                
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
    </div>
<!-- end row -->
@endsection



@section('script')
<!-- datatable js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
@endsection

@section('script-bottom')
<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
